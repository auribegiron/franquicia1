package co.com.franquicia1.dto;

public class Compra {
	
private double valorCompra;
private Tarjeta tarjeta;
public double getValorCompra() {
	return valorCompra;
}
public void setValorCompra(double valorCompra) {
	this.valorCompra = valorCompra;
}
public Tarjeta getTarjeta() {
	return tarjeta;
}
public void setTarjeta(Tarjeta tarjeta) {
	this.tarjeta = tarjeta;
}

}
