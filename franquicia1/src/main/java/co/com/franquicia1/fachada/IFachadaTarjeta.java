package co.com.franquicia1.fachada;

import co.com.franquicia1.dto.DatosGeneracionTarjeta;
import co.com.franquicia1.dto.Tarjeta;
import co.com.franquicia1.dto.Compra;;

public interface IFachadaTarjeta {
	
	public Tarjeta crearTarjeta(DatosGeneracionTarjeta datosGeneracionTarjeta);
	
	public boolean registrarCompra (Compra compra);
	public Tarjeta consultarTarjeta(String numero);

}
